# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://pecovnik@bitbucket.org/pecovnik/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/pecovnik/stroboskop/commits/7aa55590e184c459db8ff9cb3b06137580525aa4?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/pecovnik/stroboskop/commits/c5f0a7d77943f1ebbbde4be24a450bcf5c885968

Naloga 6.3.2:
https://bitbucket.org/pecovnik/stroboskop/commits/db6cd3cd2ebab53009d9d2f1affebb0bfdc6ba66#chg-stroboskop.html

Naloga 6.3.3:
https://bitbucket.org/pecovnik/stroboskop/commits/29e378b7fb5518833b23bcb8bfdd265ade1e78d4#chg-stroboskop.html

Naloga 6.3.4:
https://bitbucket.org/pecovnik/stroboskop/commits/3abe015035941b7e698cea8e02c4a1f6374fdeae

Naloga 6.3.5:

git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/pecovnik/stroboskop/commits/fbf7b80fc02c0c61037596eb41bab4d6c47cccee

Naloga 6.4.2:
https://bitbucket.org/pecovnik/stroboskop/commits/6afe36a5a92e07008c94f47cfd7e847ddd3119af

Naloga 6.4.3:
https://bitbucket.org/pecovnik/stroboskop/commits/e9a8bc13468534556bdccafb17026f8a883d9004

Naloga 6.4.4:
https://bitbucket.org/pecovnik/stroboskop/commits/c7439bb285cc54d9f82ff100f08e98aac3e7e257